﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    public Vector3 endPoint01;
    public Vector3 endPoint02;
    public float moveSpeed;
    public Vector3 Speed;

    private Vector3 mDirection;
    private bool mIsForward;
    
    // Use this for initialization
    void Start () {
        transform.position = endPoint01;
        Vector3 diff = endPoint02 - endPoint01;
        mDirection = diff.normalized;
        mIsForward = true;
        Speed = new Vector3(0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (mIsForward) {
			Speed = mDirection * moveSpeed;
			transform.position += Speed * Time.deltaTime;

			Vector3 diff = endPoint02 - transform.position;
			if (diff.magnitude < 0.1f) {
				mIsForward = false;
			}
        }
        else {
			Speed = mDirection * -moveSpeed;
			transform.position += Speed * Time.deltaTime;

			Vector3 diff = endPoint01 - transform.position;
			if (diff.magnitude < 0.1f) {
				mIsForward = true;
			}
        }
	}
}
