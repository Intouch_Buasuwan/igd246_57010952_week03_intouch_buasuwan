﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public static PlayerController Instance;

    public Game currentGame;
	public float moveSpeed;
    public float jumpSpeed;
	public float sprintSpeed;
	public float crouchSpeed;
	public Animator AmandaAnimator;
    public CharacterController chController;

	public bool AiEnable;
	public PlayerAi ActiveAi;

	private bool jumpDouble;
	private float mainSpeed;
    private Vector3 mSpeed;
	private Vector3 mSprint;
	private Vector3 playerLossyScale;
    private CollisionFlags mCollisionFlags;
	private MovingPlatform mCurrentMovingPlatform;

	void Awake () {
		if (Instance != null) {
			Destroy (this.gameObject);
		}

		Instance = this;
	}

    void Start () {
		mainSpeed = moveSpeed;
		ActiveAi.IsMovingRight = true;
		ActiveAi.DirectionHorizontal = 1;
        mCollisionFlags = CollisionFlags.None;
		playerLossyScale = this.transform.lossyScale;

		Application.runInBackground = true;
    }

    private bool mLastIsGrounded = false;


	void FixedUpdate () {		
		float h = 0;

		if (AiEnable) {
			h = ActiveAi.InputHorizontal;
			mCurrentMovingPlatform = null;
			mainSpeed = ActiveAi.InputPlayerSpeed;
			mSpeed.x = h * mainSpeed;

			if (ActiveAi.IsMovingRight == true) {
				this.transform.localScale = playerLossyScale;
			} else if (ActiveAi.IsMovingRight == false) {				
				this.transform.localScale = new Vector3 (playerLossyScale.x, playerLossyScale.y, -playerLossyScale.z);
			}

			if (mainSpeed == sprintSpeed) {
				AmandaAnimator.SetFloat ("Speed", 1);
				AmandaAnimator.SetBool ("Sprint", true);
			} else { 
				AmandaAnimator.SetFloat ("Speed", 0);
				AmandaAnimator.SetBool ("Sprint", false);
			}

			if (mLastIsGrounded) {				
				AmandaAnimator.SetBool ("Grounded", true);
				ActiveAi.IsInAir = false;

				if (mCurrentMovingPlatform != null) {
					if (mCurrentMovingPlatform.gameObject.tag == "MovingPlatform") {
						AmandaAnimator.SetBool ("Grounded", true);
						mSpeed.x += mCurrentMovingPlatform.Speed.x;
						Vector3 mCurrentMovingPlatformTransform = mCurrentMovingPlatform.transform.position;
						Vector3 thisTranform = this.transform.position;
						thisTranform.y = mCurrentMovingPlatformTransform.y;
					}
				}

				if (ActiveAi.InputJump) {
					AmandaAnimator.SetTrigger ("Jump");
					mSpeed.y = jumpSpeed;
					chController.Move (mSpeed * Time.deltaTime);
				}
			} else {
				AmandaAnimator.SetBool ("Grounded", false);
				ActiveAi.IsInAir = true;
				mSpeed.y += currentGame.gravity * Time.deltaTime;

				if (ActiveAi.InputJump) {
					mSpeed.y = jumpSpeed;
					chController.Move (mSpeed * Time.deltaTime);
				}
			}
		} else {
			h = Input.GetAxis ("Horizontal");
			mSpeed.x = h * mainSpeed;

			if (mLastIsGrounded) {
				AmandaAnimator.SetBool ("Grounded", true);
				jumpDouble = true;
                
                if (Input.GetKeyDown (KeyCode.UpArrow)) {
					AmandaAnimator.SetTrigger ("Jump");
					mSpeed.y = jumpSpeed;
					chController.Move (mSpeed * Time.deltaTime);
				}

				if (AmandaAnimator.GetBool ("Crouch") == false) {
					if (Input.GetKey (KeyCode.LeftShift)) {
						AmandaAnimator.SetBool ("Sprint", true);
						mainSpeed = sprintSpeed;
						if (Input.GetKeyDown (KeyCode.LeftControl)) {
							AmandaAnimator.SetTrigger ("Roll");
							AmandaAnimator.SetBool ("Crouch", true);
							AmandaAnimator.SetBool ("Sprint", false);
						}
					} else {
						AmandaAnimator.SetBool ("Sprint", false);
						mainSpeed = moveSpeed;
					}
				}

				if (AmandaAnimator.GetBool ("Sprint") == false) {
					if (Input.GetKey (KeyCode.LeftControl)) {
						AmandaAnimator.SetBool ("Crouch", true);
						mainSpeed = crouchSpeed;
					} else {
						AmandaAnimator.SetBool ("Crouch", false);
						mainSpeed = moveSpeed;
					}
				}
			} else {
				AmandaAnimator.SetBool ("Grounded", false);
				mCurrentMovingPlatform = null;
				mSpeed.y += currentGame.gravity * Time.deltaTime;

				if (mSpeed.y < currentGame.MaxFallSpeed) {
					mSpeed.y = currentGame.MaxFallSpeed;
				}

                if (Input.GetKeyDown (KeyCode.UpArrow)) {
					if (jumpDouble == true) {
						AmandaAnimator.SetTrigger ("Jump");
						mSpeed.y = jumpSpeed * 1.25f;
						chController.Move (mSpeed * Time.deltaTime);
						jumpDouble = false;
					}
				}		
			}

			if (Input.GetKey (KeyCode.RightArrow)) {
				this.transform.localScale = playerLossyScale;
				AmandaAnimator.SetFloat ("Speed", 1);
			} else if (Input.GetKey (KeyCode.LeftArrow)) {
				this.transform.localScale = new Vector3 (playerLossyScale.x, playerLossyScale.y, -playerLossyScale.z);
				AmandaAnimator.SetFloat ("Speed", 1);
			} else {
				AmandaAnimator.SetFloat ("Speed", 0);
			}

			if (mCurrentMovingPlatform != null) {
				if (mCurrentMovingPlatform.gameObject.tag == "MovingPlatform") {
					AmandaAnimator.SetBool ("Grounded", true);
					mSpeed.x += mCurrentMovingPlatform.Speed.x;
					Vector3 mCurrentMovingPlatformTransform = mCurrentMovingPlatform.transform.position;
					Vector3 thisTranform = this.transform.position;
					thisTranform.y = mCurrentMovingPlatformTransform.y;
				}
			}
		}

		chController.Move (mSpeed * Time.deltaTime);
        mLastIsGrounded = chController.isGrounded;
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
		if (hit.gameObject.tag == "BouncingPlatform") {
			mSpeed.y = hit.gameObject.GetComponent<BouncingPlatform>().jumpSpeed;
		}

		if (hit.gameObject.tag == "MovingPlatform") {
			mCurrentMovingPlatform = hit.gameObject.GetComponent<MovingPlatform> ();
			AmandaAnimator.SetBool ("Grounded", true);
		}

		if (hit.gameObject.tag == "BreakingPlatform") {
			Destroy (hit.gameObject);
		}
    }
}
