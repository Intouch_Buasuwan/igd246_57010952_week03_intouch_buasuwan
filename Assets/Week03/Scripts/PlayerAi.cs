﻿using UnityEngine;
using System.Collections;

public class PlayerAi : MonoBehaviour {

	public int DirectionHorizontal;
	public float InputHorizontal;
	public float InputPlayerSpeed;
	public bool IsInAir;
	public bool InputJump;
	public bool DoubleJump;
	public bool IsMovingRight;
	public Transform RaycastProxyAhead;
	public float RaycastDistanceAhead;
	public Transform RaycastProxyFeet;
	public float RaycastDistanceFeet;
	public Transform RaycastProxyBelow;
	public float RaycastDistanceBelow;
	public Transform RaycastProxyInFront;
	public float RaycastDistanceInFront;

	private float IsInAirTimer;

	void Start () {
		StartCoroutine (CheckPlayer ());
	}

	IEnumerator CheckPlayer() {
		while (true) {
			yield return null;
			if (PlayerController.Instance.AiEnable) {
				if (IsInAir) {
					InputJump = false;

					if (IsInAirTimer < 0.3f) {
						InputHorizontal = DirectionHorizontal;
						InputPlayerSpeed = PlayerController.Instance.sprintSpeed;
						DoubleJump = true;
					} else {
						RaycastHit plathit;
						if (Physics.Raycast (RaycastProxyBelow.position, Vector3.down, out plathit, RaycastDistanceBelow)) {
							if (plathit.transform.tag == "MovingPlatform" ||
								plathit.transform.tag == "BouncingPlatform") {
								InputHorizontal = 0;
								InputPlayerSpeed = 0;
							}
						} else {
							InputHorizontal = DirectionHorizontal;
							InputPlayerSpeed = PlayerController.Instance.sprintSpeed;
						}

						if (IsInAirTimer > 0.8f && DoubleJump == true &&
						    Physics.Raycast (RaycastProxyBelow.position, Vector3.down, RaycastDistanceBelow) == false) {
							InputJump = true;
							yield return null;
							InputJump = false;
							DoubleJump = false;
						}
					}

					IsInAirTimer += 1 * Time.deltaTime;
				} else {
					IsInAirTimer = 0;
					RaycastHit hit;

					if (Physics.Raycast (RaycastProxyInFront.position, Vector3.down, out hit, RaycastDistanceInFront)) {
						if (hit.transform.tag == "AiJumpPlatform") {
							InputJump = true;
						} else if (hit.transform.tag == "AiReversePlatform") {
							if (IsMovingRight) {
								IsMovingRight = false;
								DirectionHorizontal = -1;
							} else {
								IsMovingRight = true;
								DirectionHorizontal = 1;
							}
						}
					}
					RaycastHit plathit;
					if (Physics.Raycast (RaycastProxyBelow.position, Vector3.down, out plathit, RaycastDistanceBelow)) {
						if (plathit.transform.tag == "Finish") {
							InputHorizontal = 0;
							InputPlayerSpeed = 0;
						}
					}

					if (Physics.Raycast (RaycastProxyFeet.position, Vector3.down, RaycastDistanceFeet) == true) {
						InputHorizontal = 1;
						InputPlayerSpeed = PlayerController.Instance.sprintSpeed;
					} else if (Physics.Raycast (RaycastProxyFeet.position, Vector3.down, RaycastDistanceFeet) == false) {					
						if (Physics.Raycast (RaycastProxyAhead.position, Vector3.down, RaycastDistanceAhead) == true) {
							InputHorizontal = DirectionHorizontal;
							InputPlayerSpeed = PlayerController.Instance.sprintSpeed;
							InputJump = true;
						} else {
							InputHorizontal = 0;
							InputPlayerSpeed = 0;
						}
					}
				}

				Debug.Log (InputHorizontal);
			}
		}
	}

	void OnDrawGizmos () {
		Vector3 pAhead1 = RaycastProxyAhead.position;
		Vector3 pAhead2 = new Vector3 (pAhead1.x, pAhead1.y - RaycastDistanceAhead, pAhead1.z);
		Gizmos.DrawLine (pAhead1, pAhead2);

		Vector3 pFeet1 = RaycastProxyFeet.position;
		Vector3 pFeet2 = new Vector3 (pFeet1.x, pFeet1.y - RaycastDistanceFeet, pFeet1.z);
		Gizmos.DrawLine (pFeet1, pFeet2);

		Vector3 pBelow1 = RaycastProxyBelow.position;
		Vector3 pBelow2 = new Vector3 (pBelow1.x, pBelow1.y - RaycastDistanceBelow, pBelow1.z);
		Gizmos.DrawLine (pBelow1, pBelow2);

		Vector3 pInFront1 = RaycastProxyInFront.position;
		Vector3 pInFront2 = new Vector3 (pInFront1.x, pInFront1.y - RaycastDistanceInFront, pInFront1.z);
		Gizmos.DrawLine (pInFront1, pInFront2);
	}
}
